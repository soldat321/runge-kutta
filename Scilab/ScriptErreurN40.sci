function yprim=f(t,y)
    yprim=2*y + t;
endfunction//Définition de la fonction f
y0=0; t0=0; Nb=40; T=2;//Assigniation des valeurs pour les variables
exec('Euler.sci',-1);
exec('PointMilieu.sci',-1);
exec('RK4.sci',-1);
clf;
t=t0:T/Nb:t0+T;
y1=Euler(y0,t0,t,f);
y=-0.5*(t+0.5-0.5*exp(2*t));
plot(t,abs(y1-y));
y2=PointMilieu(y0,t0,t,f);
plot(t,abs(y2-y),'r-o');
y3=RungeKutta(y0,t0,t,f);
plot(t,abs(y3-y),'m-d');
xtitle("Evolution des erreurs des méthodes numériques à un pas pour N=40","Axe du temps t","Axe des erreurs");
legend("Méthode d''Euler", "Méthode du Point Milieu", "Méthode de Runge-Kutta Classique", 2);

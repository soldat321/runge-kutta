function yprim=f(t,y)
    yprim=-2*t*y^2;
endfunction//Définition de la fonction f
y0=1; t0=0; Nb=10; T=1;//Assigniation des valeurs pour les variables
exec('Euler.sci');
exec('PointMilieu.sci');
exec('RK4.sci');//Chargement des fonctions
x=0:0.001:1;
y=1./(1+x^2);
plot(x, y);//Calcul de la solution exacte et dessin du graphe
t=t0:T/Nb:t0+T;
y1=Euler(y0,t0,t,f);
plot(t,y1,'g--');
y2=PointMilieu(y0,t0,t,f);
plot(t,y2,'r-.o');
y3=RungeKutta(y0,t0,t,f);
plot(t,y3,'md');//Calcul des solutions approchées et dessin des graphes
xtitle("Comparaison entre les méthodes numériques à un pas pour N=10",..
"Axe du temps t","Axe des y");
legend("Solution exacte","Méthode d''Euler", "Méthode du Point Milieu",..
 "Méthode de Runge-Kutta Classique", 3);

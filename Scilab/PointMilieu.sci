function y=PointMilieu(y0, t0, t, f)
    //Programme qui calcule les solutions d'un problème de Cauchy en certains 
    //points par la méthode du Point Milieu
    //y0 est la valeur initiale que prend la solution au point t0
    //t est le vecteur des temps ou seront calculés les valeurs de la 
    //solution y. f est le second membre de l'EDO
    
    y(1)=y0;        //Initialisation du vecteur y
    n=size(t, 'c'); //Utiliser la fonction size pour trouver la taille du 
                    //vecteur t. L'argument 'c' fait donner à n le nombre de 
                    //colonnes du vecteur 
    for i = 1 : n-1
        h=t(i+1)-t(i);
        p=f(t(i)+h/2,y(i)+h*f(t(i),y(i))/2);
        y(i+1)=y(i)+h*p; //La formule de la méthode du Point Milieu
    end            
    y=y';        //On transpose le vecteur résultat
endfunction

function yprim=f(t,y)
    yprim=2*y+t;
endfunction//Définition de la fonction f
y0=0; t0=0; T=2;//Assigniation des valeurs pour les variables
exec('Euler.sci',-1);
exec('PointMilieu.sci',-1);
exec('RK4.sci',-1);
//clf;
N = 5:5:100;
Y1=zeros(20,1);
Y2=zeros(20,1);
Y3=zeros(20,1);
for i = 1 : 20
h=T/N(i);
t=t0:h:t0+T;
tic();
y1=Euler(y0,t0,t,f);
Y1(i)=toc();
tic();
y2=PointMilieu(y0,t0,t,f);
Y2(i)=toc();
tic();
y3=RungeKutta(y0,t0,t,f);
Y3(i)=toc();
end
plot(N,Y1);
plot(N,Y2,'r-o');
plot(N,Y3,'m-d');
xtitle("Temps d''utilisation du CPU des méthodes au point t=2 en fonction de la subdivision ","Axe des N","Temps de calcul de l''approximtion au point t=2 (secondes)");
legend("Méthode d''Euler", "Méthode du Point Milieu", "Méthode de Runge-Kutta Classique", 2);

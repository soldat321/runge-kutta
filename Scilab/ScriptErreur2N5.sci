function yprim=f(t,y)
    yprim=-2*t*y^2;
endfunction//Définition de la fonction f
y0=1; t0=0; Nb=5; T=1;//Assigniation des valeurs pour les variables
exec('Euler.sci');
exec('PointMilieu.sci');
exec('RK4.sci');//Chargement des fonctions
t=t0:T/Nb:t0+T;
y1=Euler(y0,t0,t,f);
y=1./(1+t^2);
plot(t,abs(y1-y));
y2=PointMilieu(y0,t0,t,f);
plot(t,abs(y2-y),'r-o');
y3=RungeKutta(y0,t0,t,f);
plot(t,abs(y3-y),'m-d');
xtitle("Evolution des erreurs des méthodes numériques à un pas pour N=5","Axe du temps t","Axe des erreurs");
legend("Méthode d''Euler", "Méthode du Point Milieu", "Méthode de Runge-Kutta Classique", 2);

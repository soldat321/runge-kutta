exec('C:\Users\Administrateur\Documents\Projet\Programmes\Scilab\Euler.sci',-1);
exec('C:\Users\Administrateur\Documents\Projet\Programmes\Scilab\PointMilieu.sci',-1);
exec('C:\Users\Administrateur\Documents\Projet\Programmes\Scilab\RK4.sci',-1);
t=t0:T/Nb:t0+T;
y1=Euler(y0,t0,t,f);
plot(t,y1,'g--');
y2=PointMilieu(y0,t0,t,f);
plot(t,y2,'r-.o');
y3=RungeKutta(y0,t0,t,f);
plot(t,y3,'md');
xtitle("Comparaison entre les méthodes numériques à un pas pour N=40","Axe du temps t","Axe des y");
legend("Solution exacte","Méthode d''Euler", "Méthode du Point Milieu", "Méthode de Runge-Kutta Classique", 2);



/*Programme qui donne la solution d'un probl�me de Cauchy par la m�thode d'Euler*/
/*et qui enregistre les valeurs obtenus dans un fichier .dat*/
#include<stdio.h>
#include<stdlib.h>

float f (float t, float y);
int main()
{
float t0,y0,T,h,p1,p2,p3,p4,tn ,yn;int N,i ; FILE* fichier=NULL;
/*t0 sera la borne inf de l'intervalle, y0 la valeur de la solution en t0. h sera le pas constant et N le nombre*/
/*d'intervalles de la subdivision.*/
/*Nous demondons � l'utilisateur de saisir les variables*/
 printf("Saisir la borne inf de votre intervalle de travail:");
 scanf("%f", &t0);
 printf("Saisir la valeur de la solution au premier point de l'intervalle:");
 scanf("%f", &y0);
 printf("Saisir la borne superieur de votre intervalle:");
 scanf("%f", &T);
 T=T-t0;
 printf("Saisir le nombre de sous-intervalles:");
 scanf("%d",&N);

 h= T/N;// La valeur du pas de la subdivision
 fichier=fopen("RK4.dat", "w+");//Ouverture du fichier
 if(fichier!=NULL)//Test de l'ouverture du fichier
 {
    tn=t0;
    yn=y0;
    printf("%f %f \n", t0 ,y0);//L'affichage de premier point t0, et la solution en ce point
    fprintf(fichier,"%f %f \n", t0, y0 );//Ecriture des premi�res valeurs dans le fichier
    for(i=1 ; i<=N ; i++)
    {
        p1=f(tn,yn);
        p2=f(tn+h/2,yn+h*p1/2);
        p3=f(tn+h/2,yn+h*p2/2);
        p4=f(tn+h,yn+h*p3);//Calcul des quatres pentes interm�diaires
        yn=yn+h*(p1+2*p2+2*p3+p4)/6;//Calcul des valeurs de la fonction par la m�thode de Rung-Kutta
        tn=tn+h;
        printf("%f %f \n", tn ,yn);//Affichage des r�sultats dans la console
        fprintf(fichier,"%f %f \n", tn, yn );//Ecriture dans le fichier
    }
}
 else {
        printf("\nCreation de fichier impossible.");//Si le fichier ne s'est pas ouvert correctement nous arr�tons le programme
       }
return 0;
}

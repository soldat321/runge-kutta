/*Programme qui donne la solution d'un probl�me de Cauchy par la m�thode d'Euler*/
/*et qui enregistre les valeurs obtenus dans un fichier .dat*/
#include<stdio.h>
#include<stdlib.h>

float f (float t, float y)
{float prim;
    prim= -2*t*y*y ;
    return prim;
}

int main()
{
float t0,y0,T,h ,tn ,yn, S, Er;int N,i ; FILE* fichier=NULL;
/*t0 sera la borne inf de l'intervalle, y0 la valeur de la solution en t0. h sera le pas constant et N le nombre*/
/*d'intervalles de la subdivision.*/
/*Nous demondons � l'utilisateur de saisir les variables*/
 printf("Saisir la borne inf de votre intervalle de travail:");
 scanf("%f", &t0);
 printf("Saisir la valeur de la solution au premier point de l'intervalle:");
 scanf("%f", &y0);
 printf("Saisir la borne superieur de votre intervalle:");
 scanf("%f", &T);
 T=T-t0;
 printf("Saisir le nombre de sous-intervalles:");
 scanf("%d",&N);

 h= T/N;// La valeur du pas de la subdivision
 fichier=fopen("EulerErreur10.dat", "w+");//Ouverture du fichier
 if(fichier!=NULL)//Test de l'ouverture du fichier
 {
    tn=t0;
    yn=y0;
    S=1/(1+tn*tn);
    Er=yn-S;
    printf("%f %f \n", t0 ,Er);//L'affichage du premier point t0, et la solution en ce point
    fprintf(fichier,"%f %f \n", t0, Er );//Ecriture des premi�res valeurs dans le fichier
    for(i=1 ; i<=N ; i++ )
    {
        yn=yn+f(tn,yn)*h;//Calcul des valeurs de la fonction par la m�thode d'Euler,ici y_n+1
        tn=tn+h;//Calcul de t_n+1
        S=1/(1+tn*tn);
        Er=yn-S;
        printf("%f %f \n", tn , Er);//Affichage des r�sultats dans la console
        fprintf(fichier,"%f %f \n", tn, Er );//Ecriture dans le fichier
    }
}
 else {
        printf("\nCreation de fichier impossible.");//Si le fichier ne s'est pas ouvert correctement nous arr�tons le programme
       }
  return 0;
}
